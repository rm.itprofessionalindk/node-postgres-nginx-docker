const keys = require('./keys');

module.exports = {
  development: {
    debug: true,
    client: 'pg',
    connection: {
      user: keys.POSTGRES_USER,
      password: keys.POSTGRES_PASSWORD,
      database: keys.POSTGRES_DB
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: './db/migrations',
    },
    seeds: {
      directory: './db/seeds',
    },
  },
  test: {
    client: 'pg',
    connection: {
      user: keys.POSTGRES_USER,
      password: keys.POSTGRES_PASSWORD,
      database: keys.POSTGRES_DB
    },
    migrations: {
      directory: './db/migrations',
    },
    seeds: {
      directory: './db/seeds',
    },
  },
};
  