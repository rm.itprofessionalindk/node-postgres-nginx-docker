module.export = {
   pgHost: process.env.POSTGRES_HOST,
   PgUser: process.env.POSTGRES_USER,
   pgPassword: process.env.POSTGRES_PASSWORD,
   pgDatabase: process.env.POSTGRES_DB
}
